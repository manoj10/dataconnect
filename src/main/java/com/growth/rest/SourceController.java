package com.growth.rest;

import java.util.Collection;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class SourceController {
	
	@Autowired
	SourceRepository sourcerepo;
	
	@RequestMapping("/findall")
	public Collection<Source> allSources(){
		return this.sourcerepo.findAll();
	}
	
	@RequestMapping("/hive")
	public List<JSONObject> hive(@RequestParam(value="query", required=false) String query,
			  @RequestParam(value="sourceid", required=false) Long sourceId){
		
		Source currentsource = this.sourcerepo.findOne(sourceId);
		System.out.println(
		"query: " + query +
				" , sourceid: " + sourceId + " ,connection: " +
				currentsource.getConnection()+ " ,username: "+
				currentsource.getUsername()+ " ,password: "+
				currentsource.getPassword());
		
		return HiveExecute.execute(
				query, currentsource.getConnection(),
				currentsource.getUsername(),
				currentsource.getPassword()
				);
		
	}
	
	
}
