package com.growth.rest;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


@Entity
@Table(name = "Source")
public class Source implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;

	private String connection;
	
	private String username;
	
	private String password;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getConnection() {
		return connection;
	}

	public void setConnection(String connection) {
		this.connection = connection;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	protected Source() {
		
	}
	
	public Source(String connection, String username, String password) {
		this.connection = connection;
		this.username = username;
		this.password = password;
	}

	@Override
	public String toString() {
		return "Source [id=" + id + ", connection=" + connection + ", username=" + username + ", password=" + password
				+ "]";
	}
	
}
