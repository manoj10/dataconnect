package com.growth.rest;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SourceRepository extends JpaRepository<Source, Long>{
	List<Source> findByUsername(String username);
}
