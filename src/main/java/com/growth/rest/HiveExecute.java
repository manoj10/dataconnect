package com.growth.rest;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

import java.sql.DriverManager;


public class HiveExecute {
  private static String driverName = "org.apache.hive.jdbc.HiveDriver";
  
  public static List<JSONObject> execute(String q, String connection, String username, String password){
	  
		Connection c = null;
	      Statement stmt = null;
	      System.out.println("Starting execution");
	      List<JSONObject> data = new ArrayList<JSONObject>();
	      try {
	         Class.forName(driverName);
	         c = DriverManager.getConnection(connection, username, password);
	         System.out.println("Opened database successfully");
	         String query = q;
	         System.out.println( "Query = " + query );
	         stmt = c.createStatement();
	         ResultSet rs = stmt.executeQuery( query );
	         data = getFormattedResult(rs);
	         System.out.println(data.toString());
	         rs.close();
	         stmt.close();
	         c.close();
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
	         System.exit(0);
	      }
	      System.out.println("Operation done successfully");
		return data;
	}
	
	protected static List<JSONObject> getFormattedResult(ResultSet rs) {
	    List<JSONObject> resList = new ArrayList<JSONObject>();
	    try {
	        // get column names
	        ResultSetMetaData rsMeta = rs.getMetaData();
	        int columnCnt = rsMeta.getColumnCount();
	        List<String> columnNames = new ArrayList<String>();
	        List<String> typeNames = new ArrayList<String>();
	        for(int i=1;i<=columnCnt;i++) {
	            columnNames.add(rsMeta.getColumnName(i));
	            typeNames.add(rsMeta.getColumnTypeName(i));
	        }

	        while(rs.next()) { // convert each object to an human readable JSON object
	        	JSONObject obj = new JSONObject();
	            for(int i=1;i<=columnCnt;i++) {
	            	
	                String key = columnNames.get(i - 1);
	                if(typeNames.get(i - 1) == "int8"){
	                	obj.put(key, rs.getLong(i));
	                }else if(typeNames.get(i - 1) == "numeric" || typeNames.get(i - 1) == "float8"){
	                	obj.put(key, rs.getFloat(i));
	                }else{
	                	obj.put(key, rs.getString(i));
	                }
					
	            }
	            resList.add(obj);
	        }
	    } catch(Exception e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            rs.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
	    return resList;
	}
	
}
